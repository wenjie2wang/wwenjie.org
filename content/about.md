---
title: About
---

I started this website in 2016 for personal [notes](/notes) on statistics,
data science, programming, and Emacs so that my future self could easily
review.  I would be glad if it turns out to be of help to others
as well.  However, everything posted here is just my personal humble point of
view and subject to any kind of changes or corrections.  Thus, please do not
take everything for granted for your own sake.  Comments and suggestions are
welcome.


Technically speaking, this website is mainly powered by [Hugo][hugo-url] and
hosted by [Netlify][netlify-url].  The source code is available at
[GitLab][gitlab-url].  The layouts are based on the [Lanyon][lanyon-url] theme
with some minor customization.  The comment section are powered by
[Disqus][disqus-url].


[hugo-url]: https://gohugo.io/
[netlify-url]: https://netlify.com/
[lanyon-url]: https://github.com/poole/lanyon/
[gitlab-url]: https://gitlab.com/wenjie2wang/wwenjie.org/
[disqus-url]: https://disqus.com/
