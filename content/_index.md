# Welcome

I am a senior research scientist working at [Eli Lilly and Company][lilly-url].
My recent works involve innovative diabetes research and drug discovery by
statistical learning.

I earned my Ph.D. degree in statistics from the [University of
Connecticut][uconn] in Summer 2019.
My dissertation was on integrative survival analysis with application to suicide
risk, co-advised by Dr. [Kun Chen][kun-chen] and Dr. [Jun Yan][jun-yan].
I earned my B.S. degree in Statistics and an outstanding graduate award at
the [Tongji University][tongji], Shanghai, China in 2014.

I like statistical thinking and have great enthusiasm for scientific computing
mostly in <span class="proglang">R</span> and <span class="proglang">C++</span>.
Consequently, I have been developing and maintaining several
<span class="proglang">R</span> [packages](/software)
for some research projects in which I have participated or been interested.
I believe it is an exciting and challenging world for statisticians
or data scientists.

I am a big fan of open-source software and I enjoy using [Emacs][emacs] and
[Arch Linux][archlinux] every day.
I would like to contribute more to open-source efforts as I learn more.

One of my favorite statistical concepts is martingale learned from probability
theory since

> In a sense life itself is a martingale.

Formulated by the Nobel Prize laureate of 2002, Imre Kert‌&#233;sz, in his book
*Ad kudarc* with the Swedish title *Fiasko*:

> It is not the future that is expecting me, just the next moment, the future
> does not exist, it is nothing but a perpetual ongoing, a present now. The
> prediction for my future---is the quality of my present.


[lilly-url]: https://www.lilly.com/
[uconn]: https://stat.uconn.edu/
[tongji]: https://en.tongji.edu.cn/
[emacs]: https://www.gnu.org/software/emacs/
[archlinux]: https://www.archlinux.org/
[cv]: /docs/cv_wenjie_wang.pdf
[kun-chen]: https://kun-chen.uconn.edu/
[jun-yan]: https://stat.uconn.edu/jun-yan/
