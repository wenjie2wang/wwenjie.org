// show the sidebar if the screen size is large enough
// otherwise, hide it by default
function open_sidebar() {
  $("#sidebar-checkbox").prop('checked', true);
}
function close_sidebar() {
  $("#sidebar-checkbox").prop('checked', false);
}
function toggle_sidebar() {
  var sidebar_checkbox = $("#sidebar-checkbox");
  sidebar_checkbox.prop('checked', ! sidebar_checkbox.prop('checked'));
}
// $(document).ready(function() {
//   if ($(window).width() > 970) {
//     open_sidebar();
//   }
// });
// $(window).resize(function() {
//   if ($(window).width() > 970) {
//     open_sidebar();
//   } else {
//     close_sidebar();
//   }
// });
