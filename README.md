## About

[![Netlify Status][ns]](https://wwenjie.org)

This repository contains source code and static contents for my personal
website at [wwenjie.org](https://wwenjie.org/).  The theme and some
project pages are included as submodules.  Static media files are tracked as
*large files* by [Git LFS][git-lfs-url].  The site is mainly powered by
[Hugo][hugo-url] and hosted by [Netlify][netlify-url].


## License

The content of all pages is licensed under [CC BY-NC-SA 4.0][cc-by-nc-sa-4].

[git-lfs-url]: https://git-lfs.github.com/
[cc-by-nc-sa-4]: https://creativecommons.org/licenses/by-nc-sa/4.0/
[hugo-url]: https://gohugo.io/
[netlify-url]: https://netlify.com/
[ns]: https://api.netlify.com/api/v1/badges/36734c95-381c-4eb7-b2aa-cb65b5d0963d/deploy-status


